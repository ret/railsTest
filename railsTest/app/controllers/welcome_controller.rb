class WelcomeController < ApplicationController
  def index
    @articles = Article.all
    @articles = Article.page(params[:articles]).per(8).order("title")
  end

  def show
    @user = User.all
    @article = Article.find(params[:id]).picture
    @article = Article.find(params[:id])
  end
end
