class ArticlesController < ApplicationController

  def index
    @articles = Article.all
    @articles = Article.page(params[:articles]).per(8).order("title")
    @profile_pic = User.find(session[:user_id]).profile_pic
    if session[:user_id] != nil
      @sessName = User.find(session[:user_id]).firstname
    else
      @sessName = "Guest"
    end
  end

  def upvote
    @article = Article.find(params[:id])
    @article.votes.create
    redirect_to (root_path)
  end

  def show
    @user = User.all
    @article = Article.find(params[:id]).picture
    @profile_pic = User.find(session[:user_id]).profile_pic
    @article = Article.find(params[:id])
    if session[:user_id] != nil
      @sessName = User.find(session[:user_id]).firstname
    else
      @sessName = "Guest"
    end
  end

  def new
    @article = Article.new
    @profile_pic = User.find(session[:user_id]).profile_pic
    if session[:user_id] != nil
      @sessName = User.find(session[:user_id]).firstname
    else
      @sessName = "Guest"
    end
  end

  def edit
    @article = Article.find(params[:id])
    @profile_pic = User.find(session[:user_id]).profile_pic
    if session[:user_id] != nil
      @sessName = User.find(session[:user_id]).firstname
    else
      @sessName = "Guest"
    end
  end

  def create
    @article = Article.new(article_params)
    @profile_pic = User.find(session[:user_id]).profile_pic

    if @article.save
      redirect_to @article
    else
      render 'new'
    end
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to articles_path
  end


  private
  def article_params
    params.require(:article).permit(:title, :text, :picture)
  end
end
