class UsersController < ApplicationController
  before_filter :login_required, only: [:edit, :update]

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to :log_in, :notice => "Signed up!"
    else
      render "new"
    end
  end

  def edit
    @profile_pic = User.find(session[:user_id]).profile_pic
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to :articles
    else
      render 'edit'
    end
  end

  def login
  end

  def process_login
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      redirect_to :articles, :notice => "Logged in!"
    else
      flash.now.alert = "Invalid email or password"
      render "login"
    end
  end

  def my_account
    @profile_pic = User.find(session[:user_id]).profile_pic
  end

  def logout
    session[:user_id] = nil
    redirect_to :log_in, :notice => "Logged out!"
  end

  def user_params
    params.require(:user).permit(:email, :firstname, :lastname, :password, :profile_pic)
  end
end