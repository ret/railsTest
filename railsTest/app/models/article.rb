class Article < ActiveRecord::Base
  belongs_to :user
  mount_uploader :picture, ImageUploader
  has_many :votes, dependent: :destroy
  has_many :comments, dependent: :destroy
  validates :title, presence: true,
            length: { minimum: 5 }
end
